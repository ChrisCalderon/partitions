/*  Partitions
    Copyright (C) 2021  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    To contact the author, send an email to calderonchrisitan73 AT gmail DOT com
*/
#include "partitions.h"


struct Context_s {
  size_t row_count;
  size_t *row_lengths;
  mpz_t **rows;
};


static bool grow_Context(Context ctx, size_t row, size_t length);


Context
new_Context(){
  Context ctx = malloc(sizeof(struct Context_s));
  ctx->row_count = 0;
  ctx->row_lengths = NULL;
  ctx->rows = NULL;
  if(!grow_Context(ctx, 0, 1)){
    free(ctx);
    return NULL;
  }
  mpz_init_set_ui(ctx->rows[0][0], 1UL); // base case for restricted partition function. 
  return ctx;
}


void
free_Context(Context ctx)
{
  size_t i;
  size_t j;
  for(i = 0; i < ctx->row_count; i++){
    for(j = 0; j < ctx->row_lengths[i]; j++)
      mpz_clear(ctx->rows[i][j]);
    free(ctx->rows[i]);
  }
  free(ctx->rows);
  free(ctx->row_lengths);
  free(ctx);
}


static bool
grow_Context(Context ctx, size_t row, size_t length)
{
  if((row < ctx->row_count) && (length <= ctx->row_lengths[row]))
    return true;

  if(row >= ctx->row_count){
    size_t new_row_count = row + 1;
    size_t *row_lengths = realloc(ctx->row_lengths, new_row_count * sizeof *row_lengths);
    if(!row_lengths)
      return false;

    mpz_t **rows = realloc(ctx->rows, new_row_count * sizeof *rows);
    if(!rows)
      return false;

    
    memset(row_lengths + ctx->row_count, 0, (new_row_count - ctx->row_count) * sizeof(size_t));
    memset(rows + ctx->row_count, 0, (new_row_count - ctx->row_count) * sizeof(mpz_t *));
    ctx->row_count = new_row_count;
    ctx->row_lengths = row_lengths;
    ctx->rows = rows;
  }

  if(length > ctx->row_lengths[row]){
    mpz_t *new_row = realloc(ctx->rows[row], length * sizeof *new_row);
    if(!new_row)
      return false;

    size_t i;
    for(i = ctx->row_lengths[row]; i < length; i++)
      mpz_init(new_row[i]);
    
    ctx->rows[row] = new_row;
    ctx->row_lengths[row] = length;
  }

  return true;
}


void
partitions_error(int err)
{
  switch(err){
  case -1:
    puts("NULL Context not allowed.");
    break;

  case -2:
    puts("Not enough memory.");
    break;
    
  default:
    printf("Unhandled error result: %d\n", err);
  }
}


int
partitions(int n, int k, mpz_t result, Context ctx)
{
  /* The restricted partition counting function.

     This function counts the number of ways write the number
     n as the sum of k summands.
  */
  
  if(!ctx)
    // NULL ctx not allowed!
    return -1;

  if(n < 0 || k < 0){
    mpz_set_ui(result, 0UL);
    return 1;
  }
  
  if((n == 0) ^ (k == 0)){
    mpz_set_ui(result, 0UL);
    return 1;
  }

  if(k > n){
    mpz_set_ui(result, 0UL);
    return 1;
  }
  
  if(!grow_Context(ctx, n, k + 1))
    // Not enough memory!
    return -2;

  if(mpz_cmp_ui(ctx->rows[n][k], 0UL)){
    mpz_set(result, ctx->rows[n][k]);
    return 1;
  }

  if(k == n){
    mpz_set_ui(ctx->rows[n][k], 1UL);
    mpz_set_ui(result, 1UL);
    return 1;
  }

  if(k == 1){
    mpz_set_ui(ctx->rows[n][k], 1UL);
    mpz_set_ui(result, 1UL);
    return 1;
  }

  if(k == 2){
    mpz_set_ui(ctx->rows[n][k], n/2);
    mpz_set_ui(result, n/2);
    return 1;
  }

  if(k == 3){
    div_t check = div(n, 6);
    int q = check.quot;
    unsigned long result_ui;
    switch(check.rem){

    case 0:
      result_ui = 3*q*q;
      break;
      
    case 1:
      result_ui = 3*q*q + q;
      break;
      
    case 2:
      result_ui = 3*q*q + 2*q;
      break;

    case 3:
      result_ui = 3*q*q + 3*q + 1;
      break;

    case 4:
      result_ui = 3*q*q + 4*q + 1;
      break;

    case 5:
      result_ui = 3*q*q + 5*q + 2;
      break;

    }
    mpz_set_ui(ctx->rows[n][k], result_ui);
    mpz_set_ui(result, result_ui);
    return 1;
  }

  mpz_t partial_result1, partial_result2;
  mpz_inits(partial_result1, partial_result2, NULL);
  int result_code1 = partitions(n - k, k, partial_result1, ctx);
  int result_code2 = partitions(n - 1, k - 1, partial_result2, ctx);
  if((result_code1 == 1) && (result_code2 == 1)){
    mpz_add(ctx->rows[n][k], partial_result1, partial_result2);
    mpz_set(result, ctx->rows[n][k]);
    mpz_clears(partial_result1, partial_result2, NULL);
    return 1;
  } else {
    mpz_clears(partial_result1, partial_result2, NULL);
    return -2;
  }
}
