/*  Partitions
    Copyright (C) 2021  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    To contact the author, send an email to calderonchrisitan73 AT gmail DOT com
*/
#ifndef PARTITIONS_H
#define PARTITIONS_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <gmp.h>

typedef struct Context_s *Context;
Context new_Context();
void free_Context(Context ctx);
int partitions(int n, int k, mpz_t result, Context ctx);
void partitions_error(int err);
#endif
