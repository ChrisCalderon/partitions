# Partitions

A C program which calculates restricted partition counts.

Building requires GMP to be installed, and will likely require modification of the Makefile to build properly on your system, unless you are running macOS and you have Xcode, the Developer Tools, and MacPorts installed.
