/*  Partitions
    Copyright (C) 2021  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    To contact the author, send an email to calderonchrisitan73 AT gmail DOT com
*/
#include "partitions.h"


int
main(int argc, char **argv)
{
  if(argc != 3){
    printf("Incorrect argument count. Need 3, you gave %d.\n", argc);
    return 1;
  }

  int n = atoi(argv[1]);
  int k = atoi(argv[2]);
  Context ctx = new_Context();
  mpz_t result;
  mpz_init(result);
  int return_code = partitions(n, k, result, ctx);
  int code;
  if(return_code < 0){
    partitions_error(return_code);
    code = 1;
  } else {
    char *result_str = mpz_get_str(NULL, 10, result);
    printf("p(%d, %d) = %s\n", n, k, result_str);
    free(result_str);
    code = 0;
  }
  free_Context(ctx);
  mpz_clear(result);
  return code;
}
