objects = main.o partitions.o
CC = clang
CPPFLAGS = -I/opt/local/include
LDFLAGS = -L/opt/local/lib
CFLAGS = -std=c99 -O3 -Wall -Werror -pedantic

partitions: $(objects)
	$(CC) $(CFLAGS) $(LDFLAGS) -o partitions $(objects) -lgmp

main.o: main.c partitions.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c main.c

partitions.o: partitions.c partitions.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c partitions.c

clean:
	rm partitions $(objects) &>/dev/null
